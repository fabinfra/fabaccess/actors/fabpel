#!/usr/bin/env python3
import random
import argparse
import paho.mqtt.client as mqtt
import time

def main(args):
    client = mqtt.Client("fabpel_actor_%05d" % random.randint(0, 99999))
    
    if args.user is not None:
        if args.password is None:
            print("User set, but the password not")
            exit(-1)
        client.username_pw_set(args.user, args.password)

    client.connect(args.host, args.port)
    
    new_state = args.state
    if new_state == "free":
        client.publish("fabpel/%05d/00002/static" % int(args.fabpel),  b"\x00\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/static" % int(args.fabpel),  b"\x00\x00\xFF", qos=1)
    elif new_state == "inuse":
        client.publish("fabpel/%05d/00002/pulse" % int(args.fabpel),  b"\x00\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/pulse" % int(args.fabpel),  b"\x00\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/pulse" % int(args.fabpel),  b"\x00\xFF\x00", qos=1)
    elif new_state == "tocheck":
        client.publish("fabpel/%05d/00002/blink" % int(args.fabpel),  b"\xFF\xFF\xFF", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/blink" % int(args.fabpel),  b"\xFF\xFF\xFF", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/blink" % int(args.fabpel),  b"\xFF\xFF\xFF", qos=1)
    elif new_state == "blocked":
        client.publish("fabpel/%05d/00002/static" % int(args.fabpel),  b"\xFF\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/static" % int(args.fabpel),  b"\x00\x00\xFF", qos=1)
    elif new_state == "disabled":
        client.publish("fabpel/%05d/00002/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/static" % int(args.fabpel),  b"\xFF\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
    elif new_state == "reserved":
        client.publish("fabpel/%05d/00002/static" % int(args.fabpel),  b"\xFF\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/static" % int(args.fabpel),  b"\x00\x00\xFF", qos=1)
    elif new_state == "totakeover":
        client.publish("fabpel/%05d/00002/blink" % int(args.fabpel),  b"\x00\xFF\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00001/static" % int(args.fabpel),  b"\x00\x00\x00", qos=1)
        time.sleep(0.1)
        client.publish("fabpel/%05d/00000/static" % int(args.fabpel),  b"\x00\x00\xFF", qos=1)
    elif new_state == "raw":
        pass # Do nothing
    else:
        print("Process actor called with unknown state %s" % new_state)
        client.disconnect()
        exit(-1)
    client.disconnect()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="MQTT Server Address", required=True)
    parser.add_argument("--port", help="MQTT Server Port", nargs='?', default=1883, type=int)
    parser.add_argument("--user", help="MQTT User (optional)")
    parser.add_argument("--password", help="MQTT Password (optional)")
    parser.add_argument("--fabpel", help="Fabpel ID", required=True)

    parser.add_argument("name", help="Name of Actor in BFFH")

    subparsers = parser.add_subparsers(required=True, dest="state")
    
    parser_free = subparsers.add_parser("free")
    
    parser_inuse = subparsers.add_parser("inuse")
    parser_inuse.add_argument("userid", help="The user that is now using the machine")

    parser_tocheck = subparsers.add_parser("tocheck")
    parser_tocheck.add_argument("userid", help="The user that should go check the machine")

    parser_blocked = subparsers.add_parser("blocked")
    parser_blocked.add_argument("userid", help="The user that marked the machine as blocked")

    parser_disabled = subparsers.add_parser("disabled")

    parser_reserved = subparsers.add_parser("reserved")
    parser_reserved.add_argument("userid", help="The user that reserved the machine")

    parser_raw = subparsers.add_parser("raw")
    parser_raw.add_argument("data", help="Raw data for for this actor")

    args = parser.parse_args()
    main(args)
